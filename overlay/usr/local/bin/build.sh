#!/bin/sh

readonly REPODIR=$CI_PROJECT_DIR

msg() {
	local color=${2:-green}
	case "$color" in
		red) color="31";;
		green) color="32";;
		yellow) color="33";;
		blue) color="34";;
		*) color="32";;
	esac
	printf "\033[1;%sm>>>\033[1;0m %s\n" "$color" "$1" | xargs >&2
}

if ! [ -f "$HOME"/.abuild/alpine-infra.rsa ]; then
    msg "Generating key"
    abuild-keygen -n
    mv "$HOME"/.abuild/*.rsa "$HOME"/.abuild/alpine-infra.rsa
    mv "$HOME"/.abuild/*.rsa.pub "$HOME"/.abuild/alpine-infra.rsa.pub
    echo 'PACKAGER_PRIVKEY="/home/buildozer/.abuild/alpine-infra.rsa"' >>"$HOME"/.abuild/abuild.conf
fi

sudo cp "$HOME"/.abuild/alpine-infra.rsa.pub /etc/apk/keys/

msg "Upgrading system"
sudo apk upgrade -U

msg "Building repository"
buildrepo -a "$REPODIR" -p infra
